﻿using Architecture.CommonModule.ReactiveProperties.Base;

namespace Architecture.CommonModule.ReactiveProperties
{
	public class BoolReactive : SimpleReactiveProperty<bool>
	{
		public BoolReactive()
		{
		}

		public BoolReactive(bool value) : base(value)
		{
		}
	}
}