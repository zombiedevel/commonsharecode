﻿using Architecture.CommonModule.ReactiveProperties.Base;

namespace Architecture.CommonModule.ReactiveProperties
{
	public class FloatReactive : SimpleReactiveProperty<float>, INumericProperty
	{
		public float GetFloatValue()
		{
			return Value;
		}

		public void SetFromFloat(float value)
		{
			Value = value;
		}
	}
}
