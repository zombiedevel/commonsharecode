﻿using System;
using Architecture.CommonModule.ReactiveProperties.Base;

namespace Architecture.CommonModule.ReactiveProperties
{
	public class DecimalReactive : SimpleReactiveProperty<decimal>, INumericProperty
	{
		public float GetFloatValue()
		{
			return Convert.ToInt32(Value);
		}

		public void SetFromFloat(float value)
		{
			Value = Convert.ToDecimal(Value);
		}
	}
}