﻿using System;
using Architecture.CommonModule.ReactiveProperties.Base;

namespace Architecture.CommonModule.ReactiveProperties
{
	public class IntReactive : SimpleReactiveProperty<int>, INumericProperty
	{
		public float GetFloatValue()
		{
			return Convert.ToSingle(Value);
		}

		public void SetFromFloat(float value)
		{
			Value = Convert.ToInt32(value);
		}
	}
}