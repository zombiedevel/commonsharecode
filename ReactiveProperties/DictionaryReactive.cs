﻿using System;
using System.Collections;
using System.Collections.Generic;
using Architecture.CommonModule.ReactiveProperties.Base;

namespace Architecture.CommonModule.ReactiveProperties
{
	public class DictionaryReactive<TKey, TValue> : ReactiveProperty, IEnumerable<TKey>
	{
		private readonly Dictionary<TKey, TValue> _dic = new Dictionary<TKey, TValue>();

		public Action<Dictionary<TKey, TValue>> OnValueChanged;

		public TValue this[TKey index]
		{
			get
			{
				return _dic[index];
			}
			set
			{
				_dic[index] = value;
				NotifyValueChanged();
			}
		}

		public int Count
		{
			get
			{
				return _dic.Count;
			}
		}

		public void Add(TKey key, TValue obj)
		{
			_dic.Add(key, obj);
			NotifyValueChanged();
		}

		public void Remove(TKey key)
		{
			_dic.Remove(key);
			NotifyValueChanged();
		}

		public void Clear()
		{
			_dic.Clear();
			NotifyValueChanged();
		}

		private void NotifyValueChanged()
		{
			NotifyChanged();
			if (OnValueChanged != null)
			{
				OnValueChanged(_dic);
			}
		}

		IEnumerator<TKey> IEnumerable<TKey>.GetEnumerator()
		{
			return _dic.Keys.GetEnumerator();
		}

		private IEnumerator<TKey> GetEnumerator()
		{
			return _dic.Keys.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}
