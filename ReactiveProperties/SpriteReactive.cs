﻿#if UNITY
using Architecture.CommonModule.ReactiveProperties.Base;
using UnityEngine;

namespace Architecture.CommonModule.ReactiveProperties
{
	public class SpriteReactive : SimpleReactiveProperty<Sprite>
	{
	}
}
#endif