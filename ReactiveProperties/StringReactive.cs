﻿using Architecture.CommonModule.ReactiveProperties.Base;

namespace Architecture.CommonModule.ReactiveProperties
{
	public class StringReactive : SimpleReactiveProperty<string>
	{
	}
}