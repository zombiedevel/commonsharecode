﻿using System;

namespace Architecture.CommonModule.ReactiveProperties.Base
{
	public class ReactiveProperty
	{
		public Action OnChanged;

		protected void NotifyChanged()
		{
			if (OnChanged != null)
			{
				OnChanged();
			}
		}
	}

	public abstract class SimpleReactiveProperty<T> : ReactiveProperty
	{
		public Action<T> OnValueChanged;

		public T Value
		{
			get
			{
				return Get();
			}
			set
			{
				Set(value);
			}
		}

		private T _value;

		public SimpleReactiveProperty()
		{
			_value = default(T);
		}

		public SimpleReactiveProperty(T value)
		{
			_value = value;
		}

		private T Get()
		{
			return _value;
		}

		private void Set(T value)
		{
			if (IsChanged(value))
			{
				_value = value;
				NotifyValueChanged();
			}
		}

		protected virtual bool IsChanged(T newValue)
		{
			var changed = (newValue == null && _value != null)
						|| (newValue != null && _value == null)
						|| ((newValue != null) && !newValue.Equals(_value));
			return changed;
		}

		public override string ToString()
		{
			if (_value == null)
			{
				return "";
			}
			return Value.ToString();
		}

		protected void NotifyValueChanged()
		{
			NotifyChanged();
			if (OnValueChanged != null)
			{
				OnValueChanged(_value);
			}
		}
	}

	public interface INumericProperty
	{
		float GetFloatValue();
		void SetFromFloat(float value);
	}
}