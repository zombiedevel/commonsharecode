﻿using System;
using System.Collections;
using System.Collections.Generic;
using Architecture.CommonModule.ReactiveProperties.Base;

namespace Architecture.CommonModule.ReactiveProperties
{
	public class ListReactive<T> : ReactiveProperty, IEnumerable<T>
	{
		private readonly List<T> _list = new List<T>();

		public Action<List<T>> OnValueChanged;
		public Action<T> OnValueAdded;

		public T this[int index]
		{
			get
			{
				return _list[index];
			}
			set
			{
				_list[index] = value;
				NotifyValueChanged();
			}
		}

		public int Count
		{
			get
			{
				return _list.Count;
			}
		}

		public void Add(T obj)
		{
			_list.Add(obj);
			if (OnValueAdded != null)
			{
				OnValueAdded.Invoke(obj);
			}
			NotifyValueChanged();
		}

		public void RemoveAt(int obj)
		{
			_list.RemoveAt(obj);
			NotifyValueChanged();
		}

		public void Remove(T obj)
		{
			_list.Remove(obj);
			NotifyValueChanged();
		}

		public void Sort()
		{
			_list.Sort();
			NotifyValueChanged();
		}

		public void Clear()
		{
			_list.Clear();
			NotifyValueChanged();
		}

		public void AddRange(IEnumerable<T> add)
		{
			_list.AddRange(add);
			foreach (var element in add)
			{
				if (OnValueAdded != null)
				{
					OnValueAdded.Invoke(element);
				}
			}
			NotifyValueChanged();
		}

		public void Insert(int index, T obj)
		{
			_list.Insert(index, obj);
			NotifyValueChanged();
		}

		private void NotifyValueChanged()
		{
			NotifyChanged();
			if (OnValueChanged != null)
			{
				OnValueChanged(_list);
			}
		}

		IEnumerator<T> IEnumerable<T>.GetEnumerator()
		{
			return _list.GetEnumerator();
		}

		private IEnumerator<T> GetEnumerator()
		{
			return _list.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}
	}
}