using System;
using System.Collections.Generic;
using Architecture.CommonModule.FSM.States;
using Architecture.CommonModule.FSM.Transitions;

#if UNITY
using UnityEngine;
#endif

namespace Architecture.CommonModule.FSM.Machine
{
	/// <summary>
	///     ������� ����� Finite State Machine (�����-������)
	/// </summary>
	/// <typeparam name="ST_TYPE">��� ������</typeparam>
	public sealed class FSM<ST_TYPE> : FSM
		where ST_TYPE : class, IFState
	{
		private readonly Dictionary<string, ST_TYPE> _states = new Dictionary<string, ST_TYPE>();

		private readonly Dictionary<string, List<FSMTransition>> _transitions =
			new Dictionary<string, List<FSMTransition>>();

		private readonly Dictionary<string, FSMTransition> _backToHistoryTransitions =
			new Dictionary<string, FSMTransition>();

		private readonly List<string> _transitionHistory = new List<string>();

		private bool _fsmRunned;
		private readonly bool _allowSelfTransitions;


		/// <summary>
		///     ������������� �������� ������
		/// </summary>
		public string PreviousStateIdentifier { get; private set; }

		/// <summary>
		///     ������������� �������� ������
		/// </summary>
		public string CurrentStateIdentifier { get; private set; }

		/// <summary>
		///     ���������� ����� �� ��������������
		/// </summary>
		/// <param name="v">������������� ������</param>
		/// <returns></returns>
		public ST_TYPE this[object v]
		{
			get
			{
				return _states[v.ToString()];
			}
		}

		/// <summary>
		///     ���������� ������� �����
		/// </summary>
		public ST_TYPE CurrentState
		{
			get
			{
				return _states[CurrentStateIdentifier];
			}
		}

		/// <summary>
		///     ���������� ������� ����� � �������� �������� ��� � ����
		/// </summary>
		/// <typeparam name="ST_TYPE">��� ������</typeparam>
		/// <returns></returns>
		public T TryGetCurrentState<T>() where T : class
		{
			return CurrentState as T;
		}

		/// <summary>
		///     ������� ����� �������� �������
		/// </summary>
		public FSM() : this(false)
		{
		}

		public FSM(bool allowSelfTransitions)
		{
			_allowSelfTransitions = allowSelfTransitions;
		}

		/// <summary>
		///     ������� ����� �������� �������
		/// </summary>
#if UNITY
		public FSM(bool allowSelfTransitions, GameObject pinDebugVizualize) : this(allowSelfTransitions)
		{
			if (pinDebugVizualize != null)
			{
				TurnDebugMode(pinDebugVizualize);
			}
		}
#endif
		/// <summary>
		///     �������� ����� �����
		/// </summary>
		/// <param name="newState"></param>
		/// <param name="specializedState"></param>
		public void AddState(object newState, ST_TYPE specializedState = null)
		{
			if (_fsmRunned)
			{
				Error("FSM runned!");
				return;
			}


			var stateObj = specializedState ?? new FStateSimple() as ST_TYPE;
			if (stateObj == null)
			{
				Error("Call AddState() with specialized state instead FStateSimple");
			}

			_states.Add(newState.ToString(), stateObj);

			if (FSMIsDebug)
			{
				StatesListForDebug.Add(newState.ToString());
			}
		}

		public FSMTransition AddTransition(object toState)
		{
			return AddTransition(toState, toState);
		}

		/// <summary>
		///     ������� ����� ������� ����� ��������
		/// </summary>
		/// <param name="tr">��� ��������</param>
		/// <param name="toState">����� � ������� ����� ����������� �������</param>
		/// <returns>��������� �������</returns>
		public FSMTransition AddTransition(object tr, object toState)
		{
			if (_fsmRunned)
			{
				Error("FSM runned!");
				return null;
			}

			var transition = new FSMTransition(toState.ToString());
			if (!_transitions.ContainsKey(tr.ToString()))
			{
				_transitions.Add(tr.ToString(), new List<FSMTransition>());
			}
			_transitions[tr.ToString()].Add(transition);

			if (FSMIsDebug)
			{
				TransitionListForDebug.Add(transition);
			}

			return transition;
		}

		/// <summary>
		///     ������� ������� ����� �� �������
		/// </summary>
		/// <param name="tr">��� ��������</param>
		/// <returns></returns>
		public FSMTransition AddTransitionBack(object tr)
		{
			if (_fsmRunned)
			{
				Error("FSM runned!");
				return null;
			}

			var transition = new FSMTransition();
			_backToHistoryTransitions.Add(tr.ToString(), transition);

			if (FSMIsDebug)
			{
				TransitionListForDebug.Add(transition);
			}

			return transition;
		}

		/// <summary>
		///     ��������� ����� ������
		/// </summary>
		/// <param name="initState">C���� ��� �����</param>
		public void StartRestart(object initState)
		{
			if (_fsmRunned)
			{
				HistoryOfStates = new List<string>();
			}
			else
			{
				CheckOutputConsistency();
				_fsmRunned = true;

				foreach (var stType in _states)
				{
					stType.Value.ExecuteOnInit();
				}
			}


			CurrentStateIdentifier = initState.ToString();
			CurrentState.ExecuteOnEnter();

			if (FSMIsDebug)
			{
				LastState = initState.ToString();
				AddHistoryRecordIfDebugMode(initState.ToString());
			}
		}

		public void Stop()
		{
			_fsmRunned = false;
		}

		/// <summary>
		///     ��������� ����� ������ � ����������� ��������� �������
		/// </summary>
		/// <param name="ETr">��� �������� ������� ����������</param>
		public bool CallTransition(object transitionName)
		{
			var ETr = transitionName.ToString();

			if (!_fsmRunned)
			{
				Error("FSM not runned!");
				return false;
			}

			if (_backToHistoryTransitions.ContainsKey(ETr))
			{
				if (_backToHistoryTransitions[ETr].CheckTransiteFrom(CurrentStateIdentifier))
				{
					if (FSMIsDebug)
					{
						LastTransition = _backToHistoryTransitions[ETr];
					}
					return TransiteBackHistory();
				}
			}

			if (_transitions.ContainsKey(ETr))
			{
				foreach (var fsmTransition in _transitions[ETr])
				{
					if (fsmTransition.CheckTransiteFrom(CurrentStateIdentifier))
					{
						if (FSMIsDebug)
						{
							LastTransition = fsmTransition;
						}
						return SwitchState(fsmTransition.ToState);
					}
				}
			}

			return false;
		}

		/// <summary>
		///     ��������� ������ ��� ������
		/// </summary>
		public void Tick()
		{
			CurrentState.ExecuteOnTick();
		}

		private List<string> AllowedStatesFrom(FSMTransition transition)
		{
			var list = new List<string>();
			foreach (var tr in _states)
			{
				if (transition.CheckTransiteFrom(tr.Key))
				{
					list.Add(tr.Key);
				}
			}

			return list;
		}

		private bool SwitchState(object newState, bool writeToHistory = true)
		{
			if (!_allowSelfTransitions && newState.Equals(CurrentStateIdentifier))
			{
				return false;
			}

			_transitionHistory.RemoveAll(x => { return x.Equals(CurrentStateIdentifier); });
			if (writeToHistory)
			{
				_transitionHistory.Add(CurrentStateIdentifier);
			}

			this[CurrentStateIdentifier].ExecuteOnExit();
			PreviousStateIdentifier = CurrentStateIdentifier;
			CurrentStateIdentifier = newState.ToString();
			this[CurrentStateIdentifier].ExecuteOnEnter();

			if (FSMIsDebug)
			{
				LastState = newState.ToString();
				AddHistoryRecordIfDebugMode(newState.ToString());
			}
			return true;
		}

		private bool TransiteBackHistory()
		{
			if (_transitionHistory.Count == 0)
			{
				return false;
			}

			var prevState = _transitionHistory[_transitionHistory.Count - 1];
			_transitionHistory.RemoveAt(_transitionHistory.Count - 1);
			return SwitchState(prevState, false);
		}

		private void CheckOutputConsistency()
		{
			var dic = new Dictionary<string, List<string>>();
			foreach (var transition in _transitions)
			{
				foreach (var fsmTransition in transition.Value)
				{
					foreach (var fsmTransitionFromState in AllowedStatesFrom(fsmTransition))
					{
						if (!dic.ContainsKey(fsmTransitionFromState))
						{
							dic.Add(fsmTransitionFromState, new List<string>());
						}

						if (!dic[fsmTransitionFromState].Contains(transition.Key))
						{
							dic[fsmTransitionFromState].Add(transition.Key);
						}
						else
						{
							Error("Dublicate transition: " + transition.Key + " from " + fsmTransitionFromState);
						}
					}
				}
			}

			foreach (var transition in _backToHistoryTransitions)
			{
				foreach (var fsmTransitionFromState in AllowedStatesFrom(transition.Value))
				{
					if (!dic.ContainsKey(fsmTransitionFromState))
					{
						dic.Add(fsmTransitionFromState, new List<string>());
					}

					if (!dic[fsmTransitionFromState].Contains(transition.Key))
					{
						dic[fsmTransitionFromState].Add(transition.Key);
					}
					else
					{
						Error("Dublicate transition: " + transition.Key + " from " + fsmTransitionFromState);
					}
				}
			}
		}

		private void Error(object msg)
		{
#if UNITY
			Debug.LogError(msg);
#else
			throw new Exception(msg.ToString());
#endif
		}
	}
}