﻿using System;
using System.Collections.Generic;
using System.Text;
#if UNITY
using Architecture.CommonModule.FSM.FSMDebug;
#endif
using Architecture.CommonModule.FSM.States;
using Architecture.CommonModule.FSM.Transitions;
#if UNITY
using UnityEngine;
#endif

namespace Architecture.CommonModule.FSM.Machine
{
	/// <summary>
	///     Базовый FSM класс
	/// </summary>
	public abstract class FSM
	{
		public static FSM<FStateSimple> CreateSimpleFSM()
		{
			return new FSM<FStateSimple>();
		}

		protected bool FSMIsDebug;

		public List<string> StatesListForDebug;
		public string LastState;

		public List<FSMTransition> TransitionListForDebug;
		public FSMTransition LastTransition;

		public List<string> HistoryOfStates;

		protected void AddHistoryRecordIfDebugMode(string stateName)
		{
			HistoryOfStates.Add(stateName
								+ "\t" + DateTime.Now.Minute + ":" + DateTime.Now.Second + ":" + DateTime.Now.Millisecond);
		}

		/// <summary>
		///     Включение режима дебага. Включать сразу после создания FSM, перед инициализацией
		/// </summary>
#if UNITY
		protected void TurnDebugMode(GameObject attachDebuggerTo)
		{
			FSMIsDebug = true;

			StatesListForDebug = new List<string>();
			TransitionListForDebug = new List<FSMTransition>();
			LastTransition = new FSMTransition();
			HistoryOfStates = new List<string>();

			attachDebuggerTo.AddComponent<FSMVsualizer>().TrackFSMFromHere(this);
		}
#endif

		public string GetDebugInfo()
		{
			if (!FSMIsDebug)
			{
				return "Not enabled debug mode! Attach GO for vizualize in constructor";
			}

			var sb = new StringBuilder();

			sb.Append("States:\n\n");
			foreach (var st in StatesListForDebug)
			{
				if (st == LastState)
				{
					sb.Append(">>> " + st + "\n");
				}
				else
				{
					sb.Append("       " + st + "\n");
				}
			}

			sb.Append("\n\nTransitions:\n\n");
			foreach (var tr in TransitionListForDebug)
			{
				if (tr.ToString() == LastTransition.ToString())
				{
					sb.Append(">>> " + tr + "\n");
				}
				else
				{
					sb.Append("       " + tr + "\n");
				}
			}

			sb.Append("\n\nHistory:\n\n");
			for (var i = HistoryOfStates.Count - 1; i >= 0; i--)
			{
				var tr = HistoryOfStates[i];
				sb.Append("       " + tr + "\n");
			}

			return sb.ToString();
		}
	}
}