﻿#if UNITY
using UnityEngine;

namespace Architecture.CommonModule.FSM.FSMDebug
{
	public class FSMVsualizer : MonoBehaviour
	{
		public Machine.FSM _fsm;

		[TextArea(20,30)]
		public string Log;


		/// <summary>
		/// Вызывать сразу после создания FSM!
		/// </summary>
		/// <param name="fsm"></param>
		public void TrackFSMFromHere(Machine.FSM fsm)
		{
			_fsm = fsm;
		}

		public void Update()
		{
			Log = _fsm.GetDebugInfo();
		}
	}
}
#endif