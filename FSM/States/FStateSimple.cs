﻿using System;

namespace Architecture.CommonModule.FSM.States
{
	/// <summary>
	///     Простой стейт использующийся по умолчанию. Позволяет назначить действия не создавая груду классов
	/// </summary>
	public sealed class FStateSimple : IFState
	{
		public Action OnInit { get; set; }
		public Action OnEnter { get; set; }
		public Action OnTick { get; set; }
		public Action OnExit { get; set; }

		public void ExecuteOnInit()
		{
			if (OnInit != null)
			{
				OnInit();
			}
		}

		public void ExecuteOnEnter()
		{
			if (OnEnter != null)
			{
				OnEnter();
			}
		}

		public void ExecuteOnTick()
		{
			if (OnTick != null)
			{
				OnTick();
			}
		}

		public void ExecuteOnExit()
		{
			if (OnExit != null)
			{
				OnExit();
			}
		}
	}
}