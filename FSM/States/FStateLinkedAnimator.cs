﻿#if UNITY
using System;
using UnityEngine;

namespace Architecture.CommonModule.FSM.States
{
	[Serializable]
	public class FStateLinkAnimator<T> : IFState
	{
		public T State;

		private Animator _animator;

		[SerializeField]
		private string[] _randomTriggers;

		private int[] _randomTriggersInt;

		private static System.Random _rnd = new System.Random();

		public void Init(Animator animator)
		{
			_animator = animator;
			_randomTriggersInt = new int[_randomTriggers.Length];

			for (int i = 0; i < _randomTriggersInt.Length; i++)
			{
				_randomTriggersInt[i] = Animator.StringToHash(_randomTriggers[i]);
			}
		}

		public virtual void ExecuteOnInit()
		{
		}

		public virtual void ExecuteOnEnter()
		{
			var trigger = _randomTriggersInt[_rnd.Next(0, _randomTriggersInt.Length)];
			_animator.SetTrigger(trigger);
		}

		public virtual void ExecuteOnTick()
		{
		}

		public virtual void ExecuteOnExit()
		{
		}
	}
}
#endif