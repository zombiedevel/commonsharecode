using System;

namespace Architecture.CommonModule.FSM.States
{
	/// <summary>
	///     ��������� ������
	/// </summary>
	public interface IFState
	{
		/// <summary>
		///     �� �������� �������! ���������� ��� ������ ����� ������
		/// </summary>
		[Obsolete("�� �������� �������! ������������� ��� ������ �� ����� ������")]
		void ExecuteOnInit();

		/// <summary>
		///     �� �������� �������! ���������� ��� ����� � �����.
		/// </summary>
		[Obsolete("�� �������� �������! ������������� ��� ������ �� ����� ������")]
		void ExecuteOnEnter();

		/// <summary>
		///     �� �������� �������! ���������� ��� ���������� ������� ������.
		/// </summary>
		[Obsolete("�� �������� �������! ������������� ��� ������ �� ����� ������")]
		void ExecuteOnTick();

		/// <summary>
		///     �� �������� �������! ���������� ��� ������ �� ������.
		/// </summary>
		[Obsolete("�� �������� �������! ������������� ��� ������ �� ����� ������")]
		void ExecuteOnExit();
	}
}