﻿using System.Text;

namespace Architecture.CommonModule.FSM.Transitions
{
	public sealed class FSMTransition
	{
		private bool _statesFromIncluded;
		private object[] _statesFrom = new string[0];
		public readonly object ToState;

		public FSMTransition(string toState)
		{
			ToState = toState;
		}

		public FSMTransition()
		{
			_statesFrom = new string[0];
		}

		/// <summary>
		///     Переход будет работать только из указанных стейтов
		/// </summary>
		/// <param name="fromStates">Список стейтов из которых разрешен переход</param>
		public void OnlyFrom(params object[] fromStates)
		{
			_statesFrom = fromStates;
			_statesFromIncluded = true;
		}

		/// <summary>
		///     Переход будет работать для всех стейтов кроме
		/// </summary>
		/// <param name="fromStates">Список стейтов из которых запрещен переход</param>
		public void ExcludeFrom(params object[] fromStates)
		{
			_statesFrom = fromStates;
			_statesFromIncluded = false;
		}

		public bool CheckTransiteFrom(object currentState)
		{
			if (AllowFromAll())
			{
				return true;
			}

			foreach (object st in _statesFrom)
			{
				if (st.ToString().Equals(currentState.ToString()))
				{
					return _statesFromIncluded;
				}
			}

			return !_statesFromIncluded;
		}

		private bool AllowFromAll()
		{
			if (_statesFrom == null || _statesFrom.Length == 0)
			{
				return true;
			}
			return false;
		}

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();
			if (AllowFromAll())
			{
				sb.Append("ALL");
			}
			else
			{
				if (!_statesFromIncluded)
				{
					sb.Append("ALL except ");
				}
				sb.Append("[");
				for (var i = 0; i < _statesFrom.Length; i++)
				{
					var fromState = _statesFrom[i];
					if (i != 0)
					{
						sb.Append(",");
					}
					sb.Append(fromState);
				}
				sb.Append("]");
			}

			sb.Append(" > ");

			sb.Append(ToState == null ? "BACK" : ToState.ToString());
			return sb.ToString();
		}
	}
}